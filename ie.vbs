''' @see https://atmarkit.itmedia.co.jp/ait/articles/2201/14/news023.html#_ga=2.102501767.762724123.1672642673-212805210.1668493277
''' @see https://www.vba-ie.net/ieobject/application.php
''' @see https://learn.microsoft.com/en-us/previous-versions/windows/internet-explorer/ie-developer/general-info/hh826025(v=vs.85)?redirectedfrom=MSDN
''' @see https://masaog.wordpress.com/2010/01/19/internet-explorer-%E3%82%B3%E3%83%9E%E3%83%B3%E3%83%89%E3%83%A9%E3%82%A4%E3%83%B3-%E3%82%AA%E3%83%97%E3%82%B7%E3%83%A7%E3%83%B3%E3%81%AE%E3%81%BE%E3%81%A8%E3%82%81/

Dim url, objIE,urlcount
urlcount = Wscript.Arguments.Count
IF urlcount >= 1 then
    url = Wscript.Arguments(0)
    Set objIE = CreateObject("InternetExplorer.Application")
    objIE.Visible = True
    objIE.Navigate2 url
Else
    CreateObject("InternetExplorer.Application").Visible=true
End if
